// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

import 'cypress-file-upload';



       Cypress.Commands.add("login", (Username, Password) => { 
       // const username = Cypress.env('UserName');
      //const password = Cypress.env('Password');
       cy.get('#username').click()
       cy.get('#username').type('qadirectorgadmin@levelaccess.com');
       cy.get('#password').click();
       cy.get('#password').type('QADOAPass#1'); 
       Cypress.Cookies.preserveOnce(('MoodleSession', 'remember_token'))
       //cy.get('#loginbtn').should('have.text','Log In');
       cy.wait(3000)
       cy.get('#loginbtn').click({force: true})
       cy.wait(3000)
        }) 

        Cypress.Commands.add("SiteAdminLogin", (Username, Password) => { 
                  //const username = ${Cypress.env('UserName')};
                   //const password = Cypress.env('Password');
                     cy.get('#username').click()
                     cy.get('#username').type('qadirectsiteadmin@levelaccess.com');
                     cy.get('#password').click();
                     cy.get('#password').type('QADSAPass#1'); 
                     Cypress.Cookies.preserveOnce(('MoodleSession', 'remember_token'))
                     //cy.get('#loginbtn').should('have.text','Log In');
                     cy.wait(3000)
                     cy.get('#loginbtn').click({force: true})
                     cy.wait(3000)
              }) 
              