/// <reference types="cypress" />

context('Location', () => {

    before(() => {
        Cypress.Cookies.preserveOnce('MoodleSession', 'remember_token')
        cy.visit('https://integration-uni.levelaccess.io/login/index.php');
        cy.wait(3000)
        cy.login({email: 'qadirectsiteadmin@levelaccess.com', password: 'QADSAPass#1'})
        
  
    })

    it ('Inspect Main Page: verify Welcome Message and Verify logged in', () => {
      cy.get('h1').should('have.text', 'Welcome, QADirect OrgAdmin')
      cy.get('.page-header-headings > p').should('have.text','Your organization has made the courses below available to you to give you the digital accessibility knowledge you need, when you need it. Select any course on your dashboard below to start learning.')
       // cy.visit('https://integration-uni.levelaccess.io/my/')
       cy.wait(3000)
    })

    it ('Click on Uploading a File', () => {
        const file='sample.pdf';

        cy.wait(2000)
        cy.get('.fa-bars').should('be.visible').click()
        cy.wait(4000)
        cy.get('a[href=\'https://integration-uni.levelaccess.io/admin/tool/uploadaccessuser/index.php\'] > .m-l-0').click()  
        //drag n drop 
        cy.get('.filepicker-container').attachFile(file, {subjectType: 'drag-n-drop'});
        cy.wait(8000)
        //cy.get('a[href=\'https://integration-uni.levelaccess.io/draftfile.php/1171/user/draft/704775829/sample.pdf\']').should('have.text','sample.txt')
    })
     
         

});


