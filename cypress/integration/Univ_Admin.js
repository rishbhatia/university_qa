context('Location', () => {

    before(() => {
      const username = Cypress.env('UserName');
      const password = Cypress.env('Password');
      cy.visit('https://integration-uni.levelaccess.io/login/index.php');
      cy.login({email: username, password: password})
      Cypress.Cookies.preserveOnce(('MoodleSession', 'remember_token'))
      cy.get('#loginbtn').click()
    })
    

    it ('Login to the application', () => {



        //     cy.get('#username').type('qadirectorgadmin@levelaccess.com');
        // cy.get('form.MuiGrid-root > div:nth-of-type(3) svg:nth-of-type(1)').click({force: true})
        // cy.get('li[data-option-index=\'4\']').click({force: true})
        // cy.get('.MuiButton-root').should('be.visible').click({force: true})
        // cy.get('tbody.MuiTableBody-root > tr:nth-of-type(1) > td:nth-of-type(4)').should('have.text', 'Technology')
        // cy.ResetAllFilters('Status','Statusid', 'IndusryGrp', 'IndustryGrpid', 'Industry', 'IndustryId', 'Company', 'Companyid')
      })
      

    // it ('Industry Group Filter Test: ADA Filter and results check', () => {
    //      cy.get('form.MuiGrid-root > div:nth-of-type(3) svg:nth-of-type(1)').click({force: true})
    //      cy.get('li[data-option-index=\'2\']').click({force: true})
    //      cy.get('.MuiButton-root').should('be.visible').click({force: true})
    //      cy.get('tbody.MuiTableBody-root > tr:nth-of-type(1) > td:nth-of-type(4)').should('have.text', 'ADA Markets')  
    //      cy.ResetAllFilters('Status','Statusid', 'IndusryGrp', 'IndustryGrpid', 'Industry', 'IndustryId', 'Company', 'Companyid')  
    // })
       
    // it ('Industry Group Filter Test: Financial Services Filter and results check', () => {
    //     cy.get('form.MuiGrid-root > div:nth-of-type(3) svg:nth-of-type(1)').click({force: true})
    //     cy.get('li[data-option-index=\'3\']').click({force: true})
    //     cy.get('.MuiButton-root').should('be.visible').click({force: true})
    //     cy.get('tbody.MuiTableBody-root > tr:nth-of-type(1) > td:nth-of-type(4)').should('have.text', 'Financial Services')   
    //     cy.ResetAllFilters('Status','Statusid', 'IndusryGrp', 'IndustryGrpid', 'Industry', 'IndustryId', 'Company', 'Companyid') 
    // })  

    // it('Industry Filter  Test: Airlines Filter and results check',() => {
    //         //cy.get('input[aria-activedescendant=\'mui-11872-option-0\']').click({force:true})
    //         cy.get('form.MuiGrid-root > div:nth-of-type(4) button:nth-of-type(1) > span:nth-of-type(1)').click({force:true})
    //         cy.get('li[data-option-index=\'2\']').click({force: true})
    //         cy.get('.MuiButton-root').click({force: true})
    //         cy.get('tbody.MuiTableBody-root > tr:nth-of-type(1) > td:nth-of-type(5)').should('have.text','Airlines') 
    //         cy.ResetAllFilters('Status','Statusid', 'IndusryGrp', 'IndustryGrpid', 'Industry', 'IndustryId', 'Company', 'Companyid')  

    // })
    
    // it('Industry Filter Test: Banking Filter and Results check', () => {
    //     cy.get('form.MuiGrid-root > div:nth-of-type(4) button:nth-of-type(1) > span:nth-of-type(1)').click({force:true})
    //     cy.get('li[data-option-index=\'3\']').click({force:true})
    //     cy.get('.MuiButton-root').click({force: true})
    //     cy.get('tbody.MuiTableBody-root > tr:nth-of-type(1) > td:nth-of-type(5)').should('have.text','Banking')  
    //     cy.ResetAllFilters('Status','Statusid', 'IndusryGrp', 'IndustryGrpid', 'Industry', 'IndustryId', 'Company', 'Companyid')

    // })     
    
//     it('Industry Filter Test: Business Services  Filter and results check', () => {
//         cy.get('form.MuiGrid-root > div:nth-of-type(4) button:nth-of-type(1) > span:nth-of-type(1)').click({force:true})
//         cy.get('li[data-option-index=\'4\']').click({force:true})
//         cy.get('.MuiButton-root').click({force: true})
//         cy.get('tbody.MuiTableBody-root > tr:nth-of-type(1) > td:nth-of-type(5)').should('have.text','Business Services')  
//         cy.ResetAllFilters('Status','Statusid', 'IndusryGrp', 'IndustryGrpid', 'Industry', 'IndustryId', 'Company', 'Companyid')

//     })     

   
//     it('Industry Filter Test: Computer Services  Filter and results check', () => {
//         cy.get('form.MuiGrid-root > div:nth-of-type(4) button:nth-of-type(1) > span:nth-of-type(1)').click({force:true})
//         cy.get('li[data-option-index=\'5\']').click({force:true})
//         cy.get('.MuiButton-root').click({force: true})
//         cy.get('tbody.MuiTableBody-root > tr:nth-of-type(1) > td:nth-of-type(5)').should('have.text','Computer Services')  
//         cy.ResetAllFilters('Status','Statusid', 'IndusryGrp', 'IndustryGrpid', 'Industry', 'IndustryId', 'Company', 'Companyid')

//     })     

//     it('Status Filter Test: Active Filter  and results check', () => {
//       cy.get('#status-filter').click({force: true })
//       cy.get('li[data-value=\'Active\']').click()
//       cy.get('.MuiButton-root').click({force: true})
//       cy.get('.MuiTableBody-root > :nth-child(1) > :nth-child(7)').should('have.text','Active')
//       cy.ResetAllFilters('Status','Statusid', 'IndusryGrp', 'IndustryGrpid', 'Industry', 'IndustryId', 'Company', 'Companyid')

//   })


//   it('Status Filter Test: Provisioning Filter and results check', () => {
//     cy.get('#status-filter').click({force: true })
//     cy.get('li[data-value=\'Provisioning\']').click()
//     cy.get('.MuiButton-root').click({force: true})
//     cy.get('.MuiTableBody-root > :nth-child(1) > :nth-child(7)').should('have.text','Provisioning')
//     cy.ResetAllFilters('Status','Statusid', 'IndusryGrp', 'IndustryGrpid', 'Industry', 'IndustryId', 'Company', 'Companyid')
//   })

     // No Archived System: There is no Archive Results 
    // it ('Filter Inspection Test: Status Archived Filter' , () => {  
    //    cy.get('#status-filter').should('be.visible').click()
    //    cy.get('li[data-value=\'Archived\']').click()
    //    cy.get('.MuiButton-root').click({force: true})
    //    cy.get('.MuiTableBody-root > :nth-child(1) > :nth-child(7)').should('have.text','Archived')
    // })   

//  it('Company Size Filter: Extra Small 250 less  Filter and Results Check', () => {
//     cy.get('div.MuiGrid-grid-xs-11 svg:nth-of-type(1)').click({force: true})
//     cy.get('li[data-option-index=\'2\']').click({force: true})
//     cy.get('.MuiButton-label').click({force: true})
//     cy.get('.MuiTableBody-root > :nth-child(1) > :nth-child(6)').should('have.text','Extra Small (250 or Less)')
//     cy.ResetAllFilters('Status','Statusid', 'IndusryGrp', 'IndustryGrpid', 'Industry', 'IndustryId', 'Company', 'Companyid')
// }) 


// it('Company Size Filter: Small 250 or Less Filter and Results Check', () => {
//   cy.get('div.MuiGrid-grid-xs-11 svg:nth-of-type(1)').click({force: true})
//   cy.get('li[data-option-index=\'4\']').click({force: true})
//   cy.get('.MuiButton-label').click({force: true})
//   cy.get('.MuiTableBody-root > :nth-child(1) > :nth-child(6)').should('have.text','Small (250-1,000)')
//   cy.ResetAllFilters('Status','Statusid', 'IndusryGrp', 'IndustryGrpid', 'Industry', 'IndustryId', 'Company', 'Companyid')
// }) 


// it('Company Size Filter: Large 250 or Less Filter and Results Check', () => {
//   cy.get('div.MuiGrid-grid-xs-11 svg:nth-of-type(1)').click({force: true})
//   cy.get('li[data-option-index=\'3\']').click({force: true})
//   cy.get('.MuiButton-label').click({force: true})
//   cy.get('.MuiTableBody-root > :nth-child(1) > :nth-child(6)').should('have.text','Large (5,000-50,000)')
//   cy.ResetAllFilters('Status','Statusid', 'IndusryGrp', 'IndustryGrpid', 'Industry', 'IndustryId', 'Company', 'Companyid')
// }) 







});

