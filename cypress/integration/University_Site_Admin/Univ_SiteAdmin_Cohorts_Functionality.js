/// <reference types="cypress" />

context('Location', () => {

    before(() => {
     cy.visit('https://integration-uni.levelaccess.io/login/index.php');
     
     cy.SiteAdminLogin({email: 'qadirectsiteadmin@levelaccess.com', password: 'QADSAPass#1'})
     Cypress.Cookies.defaults({
        whitelist: 'MoodleSession'
      })
     Cypress.Cookies.preserveOnce('MoodleSession','remember_token')
     cy.wait(3000)

  
    })

    it ('Navigate to to Cohort Page: verify page displays Page titles', () => {
        cy.wait(4000)
        //cy.get('.fa-bars').click()
        cy.get('[href="https://integration-uni.levelaccess.io/local/accesscohort/index.php"]').click()
        cy.get('h1').should('have.text','Cohort')
        cy.get('h2').should('have.text','Available access cohorts')
        
    })

    it ('BreadCrumbs Dashboard/Cohort', () => {
        cy.get('#page-navbar').should('contains','Dashboard / Cohort')
    
    })


    it('Click on pencil Icon to chnage name for Edit the Name for ABC Bank, Verify Name Change', () => {
        
        cy.get(':nth-child(2) > .name > .inplaceeditable > .quickeditlink > .quickediticon').should('be.visible')
        cy.wait(3000)
        cy.get(':nth-child(2) > .name > .inplaceeditable > .quickeditlink > .quickediticon').click()
        cy.wait(2000)
        cy.get('.ignoredirty').type('Test123')
        cy.wait(4000)
        cy.server()
        cy.route("POST","/lib/ajax/service.php?sesskey=B5EBU7617C&info=core_update_inplace_editable").as("PostData");
        cy.get('.ignoredirty').type('{enter}','{force:true}')
        cy.wait(4000)
        cy.wait("@PostData")
        cy.get('span[data-value=\'Test 123\'] > .quickeditlink').should('have.text','Test 123');

      
              

    })


    it('Icons Display: Eye, Trash Can, Gear, Group',() => {
         cy.get(':nth-child(2) > .action').should('be.visible')

        //     //Eye Icon
        //     cy.get('[href="https://integration-uni.levelaccess.io/local/accesscohort/edit.php?id=209&returnurl=%2Flocal%2Faccesscohort%2Findex.php%3Fpage%3D0&sesskey=pZB0yYp1p3&show=1"]').should('be.visible')
       //     //Trash Icon
       //     cy.get('[href="https://integration-uni.levelaccess.io/local/accesscohort/edit.php?id=209&returnurl=%2Flocal%2Faccesscohort%2Findex.php%3Fpage%3D0&delete=1"] > .icon').should('be.visible')
       //     //Gears Icon 
       //     cy.get('[href="https://integration-uni.levelaccess.io/local/accesscohort/edit.php?id=209&returnurl=%2Flocal%2Faccesscohort%2Findex.php%3Fpage%3D0"]').should('be.visible')
       //     //Group Icon
       //     cy.get('[href="https://integration-uni.levelaccess.io/local/accesscohort/assign.php?id=209&returnurl=%2Flocal%2Faccesscohort%2Findex.php%3Fpage%3D0"] > .icon').should('be.visible')
    })


});