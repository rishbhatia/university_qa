/// <reference types="cypress" />

context('Location', () => {

    before(() => {
        Cypress.Cookies.preserveOnce('MoodleSession', 'remember_token')
        cy.visit('https://integration-uni.levelaccess.io/login/index.php');
        cy.wait(3000)
        cy.SiteAdminLogin({email: 'qadirectsiteadmin@levelaccess.com', password: 'QADSAPass#1'})
        Cypress.Cookies.preserveOnce('MoodleSession','remember_token')
  
    })

    it ('Site Admin: Header, Breadcrumb, Blocks editing display, Register your Site, Search Site and Tab Check' , () => {
        // if cy.get('#nav-drawer'). is not visible . then click  => cy.get('.fa-bars').click()

        cy.get('a[data-key=\'sitesettings\'] > .m-l-0').then(($SiteLink) => {

         if ($SiteLink.text().includes('Site administtration')) {
             //cy.get('a[data-key=\'sitesettings\'] > .m-l-0').should('be.visible')
             cy.get('a[data-key=\'sitesettings\'] > .m-l-0').click({force: true})
            }
         else {  cy.get('.fa-bars').click()
                 cy.get('a[data-key=\'sitesettings\'] > .m-l-0').click({force:true})   
        }
        })
         cy.get('h1').should('have.text','Access University')
         cy.get('h2').should('have.text','Site administration') 
         cy.get('[role="main"] > .nav').children().should('have.length', 9)
         cy.get(':nth-child(1) > .nav-link').should('have.text','Site administration')
         cy.get(':nth-child(2) > .nav-link').should('have.text','Users')
         cy.get(':nth-child(3) > .nav-link').should('have.text','Courses')
         //cy.get(':nth-child(4) > .nav-link').should('have.text','Grades')
         cy.get(':nth-child(5) > .nav-link').should('have.text','Plugins')
         cy.get(':nth-child(6) > .nav-link').should('have.text','Appearance')
         cy.get(':nth-child(7) > .nav-link').should('have.text','Server')
         cy.get(':nth-child(8) > .nav-link').should('have.text','Reports')
         cy.get(':nth-child(9) > .nav-link').should('have.text','Development')

        

     })

     it('Click on the Site Admin Tab: Verify Analytics, Competencies, Location, Language', () => {
        cy.get(':nth-child(1) > .nav-link').click()
        cy.get('#linkroot > .card > .card-block > .container > :nth-child(1) > .col-sm-9 > .list-unstyled > :nth-child(1) > a').should('have.text','Notifications')
        cy.get('#linkroot > .card > .card-block > .container > :nth-child(1) > .col-sm-9 > .list-unstyled > :nth-child(2) > a').should('have.text','Registration')
        cy.get('#linkroot > .card > .card-block > .container > :nth-child(1) > .col-sm-9 > .list-unstyled > :nth-child(3) > a').should('have.text','Advanced features')
        cy.get('#linkroot > .card > .card-block > .container > :nth-child(1) > .col-sm-9 > .list-unstyled > :nth-child(4) > a').should('have.text','Assignment upgrade helper')
        //Analytics Settings 
        cy.get('#linkroot > .card > .card-block > .container > :nth-child(3) > .col-sm-3 > :nth-child(1) > a').should('have.text','Analytics')
        cy.get('#linkroot > .card > .card-block > .container > :nth-child(3) > .col-sm-9 > .list-unstyled > :nth-child(1) > a').should('have.text','Analytics settings')
        cy.get('#linkroot > .card > .card-block > .container > :nth-child(3) > .col-sm-9 > .list-unstyled > :nth-child(2) > a').should('have.text','Analytics models')
        //Competencies 
        cy.get('#linkroot > .card > .card-block > .container > :nth-child(5) > .col-sm-3 > :nth-child(1) > a').should('have.text','Competencies')
        cy.get('#linkroot > .card > .card-block > .container > :nth-child(5) > .col-sm-9 > .list-unstyled > :nth-child(1) > a').should('have.text', 'Competencies settings')
        cy.get('#linkroot > .card > .card-block > .container > :nth-child(5) > .col-sm-9 > .list-unstyled > :nth-child(2) > a').should('have.text', 'Migrate frameworks')
        cy.get('#linkroot > .card > .card-block > .container > :nth-child(5) > .col-sm-9 > .list-unstyled > :nth-child(3) > a').should('have.text','Import competency framework')
        cy.get('#linkroot > .card > .card-block > .container > :nth-child(5) > .col-sm-9 > .list-unstyled > :nth-child(4) > a').should('have.text','Export competency framework')
        cy.get('#linkroot > .card > .card-block > .container > :nth-child(5) > .col-sm-9 > .list-unstyled > :nth-child(5) > a').should('have.text','Competency frameworks')
        cy.get('#linkroot > .card > .card-block > .container > :nth-child(5) > .col-sm-9 > .list-unstyled > :nth-child(6) > a').should('have.text','Learning plan templates')
        //Location 
        cy.get('#linkroot > .card > .card-block > .container > :nth-child(7) > .col-sm-3 > :nth-child(1) > a').should('have.text','Location')
        cy.get('#linkroot > .card > .card-block > .container > :nth-child(7) > .col-sm-9 > .list-unstyled > li > a').should('have.text','Location settings')
        //Language 
        cy.get('#linkroot > .card > .card-block > .container > :nth-child(9) > .col-sm-3 > :nth-child(1) > a').should('have.text','Language')
        cy.get('#linkroot > .card > .card-block > .container > :nth-child(9) > .col-sm-9 > .list-unstyled > :nth-child(1) > a').should('have.text','Language settings')
        cy.get('#linkroot > .card > .card-block > .container > :nth-child(9) > .col-sm-9 > .list-unstyled > :nth-child(2) > a').should('have.text','Language customisation')
        cy.get('#linkroot > .card > .card-block > .container > :nth-child(9) > .col-sm-9 > .list-unstyled > :nth-child(3) > a').should('have.text','Language packs')        
        //Security 
        cy.get('#linkroot > .card > .card-block > .container > :nth-child(11) > .col-sm-3 > :nth-child(1) > a').should('have.text','Security')
        cy.get('#linkroot > .card > .card-block > .container > :nth-child(11) > .col-sm-9 > .list-unstyled > :nth-child(1) > a').should('have.text','IP blocker')
        cy.get(':nth-child(11) > .col-sm-9 > .list-unstyled > :nth-child(2) > a').should('have.text','Site policies')
        cy.get(':nth-child(11) > .col-sm-9 > .list-unstyled > :nth-child(3) > a').should('have.text', 'HTTP security')
        cy.get(':nth-child(11) > .col-sm-9 > .list-unstyled > :nth-child(4) > a').should('have.text','Notifications')
        //Front Page 
        cy.get('#linkroot > .card > .card-block > .container > :nth-child(13) > .col-sm-3 > :nth-child(1) > a').should('have.text','Front page')
        cy.get('#linkroot > .card > .card-block > .container > :nth-child(13) > .col-sm-9 > .list-unstyled > li > a').should('have.text','Front page settings')
        // Mobile app 
        cy.get('#linkroot > .card > .card-block > .container > :nth-child(15) > .col-sm-3 > :nth-child(1) > a').should ('have.text','Mobile app')
        cy.get('#linkroot > .card > .card-block > .container > :nth-child(15) > .col-sm-9 > .list-unstyled > li > a').should('have.text','Mobile settings')
        }) 

        it('Click on the users tab, verification of Accounts. Permissions, Privacy and policies', ()=> {
         cy.get(':nth-child(2) > .nav-link').click()   
         cy.get('#linkusers > .card > .card-block > .container > :nth-child(3) > .col-sm-3 > :nth-child(1) > a').should('have.text','Accounts')
         cy.get('#linkusers > .card > .card-block > .container > :nth-child(3) > .col-sm-9 > .list-unstyled > :nth-child(1) > a').should('have.text','Browse list of users')
         cy.get('#linkusers > .card > .card-block > .container > :nth-child(3) > .col-sm-9 > .list-unstyled > :nth-child(2) > a').should('have.text','Bulk user actions')
         cy.get('#linkusers > .card > .card-block > .container > :nth-child(3) > .col-sm-9 > .list-unstyled > :nth-child(3) > a').should('have.text','Add a new user')
         cy.get('#linkusers > .card > .card-block > .container > :nth-child(3) > .col-sm-9 > .list-unstyled > :nth-child(4) > a').should('have.text','User default preferences')
         cy.get('#linkusers > .card > .card-block > .container > :nth-child(3) > .col-sm-9 > .list-unstyled > :nth-child(5) > a').should('have.text','User profile fields')
         cy.get('#linkusers > .card > .card-block > .container > :nth-child(3) > .col-sm-9 > .list-unstyled > :nth-child(6) > a').should('have.text','Cohorts')
         cy.get('#linkusers > .card > .card-block > .container > :nth-child(3) > .col-sm-9 > .list-unstyled > :nth-child(7) > a').should('have.text','Upload users')
         cy.get('#linkusers > .card > .card-block > .container > :nth-child(3) > .col-sm-9 > .list-unstyled > :nth-child(8) > a').should('have.text','Upload user pictures')
         //cy.get('#linkusers > .card > .card-block > .container > :nth-child(3) > .col-sm-9 > .list-unstyled > :nth-child(9) > a').should('have.text','Upload users by orgnizational admin')
         cy.get('#linkusers > .card > .card-block > .container > :nth-child(3) > .col-sm-9 > .list-unstyled > :nth-child(10) > a').should('have.text','Upload user pictures by organizational admin')   
         //Permissions 
         cy.get('#linkusers > .card > .card-block > .container > :nth-child(5) > .col-sm-3 > :nth-child(1) > a').should('have.text','Permissions')
         cy.get('#linkusers > .card > .card-block > .container > :nth-child(5) > .col-sm-9 > .list-unstyled > :nth-child(1) > a').should('have.text','User policies')
         cy.get('#linkusers > .card > .card-block > .container > :nth-child(5) > .col-sm-9 > .list-unstyled > :nth-child(2) > a').should('have.text','Site administrators')
         cy.get('#linkusers > .card > .card-block > .container > :nth-child(5) > .col-sm-9 > .list-unstyled > :nth-child(3) > a').should('have.text','Define roles')
         cy.get('#linkusers > .card > .card-block > .container > :nth-child(5) > .col-sm-9 > .list-unstyled > :nth-child(4) > a').should('have.text','Assign system roles')
         cy.get('#linkusers > .card > .card-block > .container > :nth-child(5) > .col-sm-9 > .list-unstyled > :nth-child(5) > a').should('have.text','Check system permissions')
         cy.get('#linkusers > .card > .card-block > .container > :nth-child(5) > .col-sm-9 > .list-unstyled > :nth-child(6) > a').should('have.text','Capability overview')
         cy.get('#linkusers > .card > .card-block > .container > :nth-child(5) > .col-sm-9 > .list-unstyled > :nth-child(7) > a').should('have.text','Assign user roles to cohort')
         cy.get('#linkusers > .card > .card-block > .container > :nth-child(5) > .col-sm-9 > .list-unstyled > :nth-child(8) > a').should('have.text','Unsupported role assignments')
         //privacy and Policies 
         cy.get('#linkusers > .card > .card-block > .container > :nth-child(7) > .col-sm-3 > :nth-child(1) > a').should('have.text','Privacy and policies')
         cy.get('#linkusers > .card > .card-block > .container > :nth-child(7) > .col-sm-9 > .list-unstyled > :nth-child(1) > a').should('have.text','Privacy settings')
         cy.get('#linkusers > .card > .card-block > .container > :nth-child(7) > .col-sm-9 > .list-unstyled > :nth-child(2) > a').should('have.text','Policy settings')
         cy.get('#linkusers > .card > .card-block > .container > :nth-child(7) > .col-sm-9 > .list-unstyled > :nth-child(3) > a').should('have.text','Data requests')
         cy.get('#linkusers > .card > .card-block > .container > :nth-child(7) > .col-sm-9 > .list-unstyled > :nth-child(4) > a').should('have.text','Data registry')
         cy.get('#linkusers > .card > .card-block > .container > :nth-child(7) > .col-sm-9 > .list-unstyled > :nth-child(5) > a').should('have.text','Data deletion')
         cy.get('#linkusers > .card > .card-block > .container > :nth-child(7) > .col-sm-9 > .list-unstyled > :nth-child(6) > a').should('have.text','Plugin privacy registry') 

        })
     
        it('Click on the Courses tab, verification of Courses and Backups', ()=> {
            cy.get(':nth-child(3) > .nav-link').click()
            //Courses 
            cy.get('#linkcourses > .card > .card-block > .container > :nth-child(1) > .col-sm-3 > :nth-child(1) > a').should('have.text','Courses')
            cy.get('#linkcourses > .card > .card-block > .container > :nth-child(1) > .col-sm-9 > .list-unstyled > :nth-child(1) > a').should('have.text','Manage courses and categories')
            cy.get('#linkcourses > .card > .card-block > .container > :nth-child(1) > .col-sm-9 > .list-unstyled > :nth-child(2) > a').should('have.text','Add a category')
            cy.get('#linkcourses > .card > .card-block > .container > :nth-child(1) > .col-sm-9 > .list-unstyled > :nth-child(3) > a').should('have.text','Restore course')
            cy.get('#linkcourses > .card > .card-block > .container > :nth-child(1) > .col-sm-9 > .list-unstyled > :nth-child(4) > a').should('have.text','Course default settings')
            cy.get('#linkcourses > .card > .card-block > .container > :nth-child(1) > .col-sm-9 > .list-unstyled > :nth-child(5) > a').should('have.text','Course request')
            cy.get('#linkcourses > .card > .card-block > .container > :nth-child(1) > .col-sm-9 > .list-unstyled > :nth-child(6) > a').should('have.text','Upload courses')
            cy.get('#linkcourses > .card > .card-block > .container > :nth-child(1) > .col-sm-9 > .list-unstyled > :nth-child(7) > a').should('have.text','Bulk edit cohorts')
            cy.get('#linkcourses > .card > .card-block > .container > :nth-child(1) > .col-sm-9 > .list-unstyled > :nth-child(8) > a').should('have.text','University course mappings')
            cy.get('#linkcourses > .card > .card-block > .container > :nth-child(1) > .col-sm-9 > .list-unstyled > :nth-child(9) > a').should('have.text','Course Delivery')
            //Backups
            cy.get('#linkcourses > .card > .card-block > .container > :nth-child(3) > .col-sm-3 > :nth-child(1) > a').should('have.text','Backups')
            cy.get('#linkcourses > .card > .card-block > .container > :nth-child(3) > .col-sm-9 > .list-unstyled > :nth-child(1) > a').should('have.text','General backup defaults')
            cy.get('#linkcourses > .card > .card-block > .container > :nth-child(3) > .col-sm-9 > .list-unstyled > :nth-child(2) > a').should('have.text','General import defaults')
            cy.get('#linkcourses > .card > .card-block > .container > :nth-child(3) > .col-sm-9 > .list-unstyled > :nth-child(3) > a').should('have.text','Automated backup setup')
            cy.get('#linkcourses > .card > .card-block > .container > :nth-child(3) > .col-sm-9 > .list-unstyled > :nth-child(4) > a').should('have.text','General restore defaults')
            
        })

        it('click on Grades tab,verification Grades and Report settings', () => {
            cy.get('a[href=\'#linkgrades\']').click()
          //Grades 
          cy.get('#linkgrades > .card > .card-block > .container > :nth-child(1) > .col-sm-3 > :nth-child(1) > a').should('have.text','Grades')   
          cy.get('#linkgrades > .card > .card-block > .container > :nth-child(1) > .col-sm-9 > .list-unstyled > :nth-child(1) > a').should('have.text','General settings')
          cy.get('#linkgrades > .card > .card-block > .container > :nth-child(1) > .col-sm-9 > .list-unstyled > :nth-child(2) > a').should('have.text','Grade category settings')
          cy.get('#linkgrades > .card > .card-block > .container > :nth-child(1) > .col-sm-9 > .list-unstyled > :nth-child(3) > a').should('have.text','Grade item settings')
          cy.get('#linkgrades > .card > .card-block > .container > :nth-child(1) > .col-sm-9 > .list-unstyled > :nth-child(4) > a').should('have.text','Scales')
          cy.get('#linkgrades > .card > .card-block > .container > :nth-child(1) > .col-sm-9 > .list-unstyled > :nth-child(5) > a').should('have.text','Letters')
          //Report Settings
          cy.get('#linkgrades > .card > .card-block > .container > :nth-child(3) > .col-sm-3 > :nth-child(1) > a').should('have.text','Report settings')
          cy.get('#linkgrades > .card > .card-block > .container > :nth-child(3) > .col-sm-9 > .list-unstyled > :nth-child(1) > a').should('have.text','Grader report')
          cy.get('#linkgrades > .card > .card-block > .container > :nth-child(3) > .col-sm-9 > .list-unstyled > :nth-child(2) > a').should('have.text','Grade history')
          cy.get('#linkgrades > .card > .card-block > .container > :nth-child(3) > .col-sm-9 > .list-unstyled > :nth-child(3) > a').should('have.text','Overview report')
          cy.get('#linkgrades > .card > .card-block > .container > :nth-child(3) > .col-sm-9 > .list-unstyled > :nth-child(4) > a').should('have.text','User report')
           





        })





});