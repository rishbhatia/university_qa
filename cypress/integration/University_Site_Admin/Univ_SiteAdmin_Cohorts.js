/// <reference types="cypress" />

context('Location', () => {

    before(() => {
     cy.visit('https://integration-uni.levelaccess.io/login/index.php');
     
     cy.SiteAdminLogin({email: 'qadirectsiteadmin@levelaccess.com', password: 'QADSAPass#1'})
     Cypress.Cookies.preserveOnce('MoodleSession','remember_token')
     cy.wait(3000)

  
    })

    it ('Navigate to to Cohort Page: verify page displays Page titles', () => {
        cy.wait(4000)
        //cy.get('.fa-bars').click()
        cy.get('[href="https://integration-uni.levelaccess.io/local/accesscohort/index.php"]').click()
        cy.get('h1').should('have.text','Cohort')
        cy.get('h2').should('have.text','Available access cohorts')
        
    })

    it ('BreadCrumbs Dashboard/Cohort', () => {
        cy.get('#page-navbar').should('contains','Dashboard / Cohort')
    
    })

    it ('Cohort Table Headers Display: Name, Cohort ID, Descriptiom Cohort Size, Source, Edit ', () => {
        cy.get('thead > tr > .c0').should('have.text', 'Name')
        cy.get('thead > tr > .c1').should('have.text', 'Cohort ID')
        cy.get('thead > tr > .c2').should('have.text', 'Description')
        cy.get('thead > tr > .c3').should('have.text', 'Cohort size')
        cy.get('thead > tr > .c4').should('have.text', 'Source')
        cy.get('thead > tr > .c5').should('have.text', 'Edit')

    })

    it('Main Content Area diplays:  Name, Cohort ID, Descrp, Cohort Size, Source, Edit, Checking Random Entries', () => {
        cy.get('#region-main-box').should('be.visible')
        cy.get(':nth-child(3) > .name').should('be.visible')
        cy.get(':nth-child(3) > .id').should('be.visible')
        cy.get(':nth-child(1) > .source').should('be.visible')
        cy.get(':nth-child(3) > .edit').should('be.visible')
    })


    it('Icons Display: Eye, Trash Can, Gear, Group',() => {
         cy.get(':nth-child(2) > .action').should('be.visible')
        //     //Eye Icon
        //     cy.get('[href="https://integration-uni.levelaccess.io/local/accesscohort/edit.php?id=209&returnurl=%2Flocal%2Faccesscohort%2Findex.php%3Fpage%3D0&sesskey=pZB0yYp1p3&show=1"]').should('be.visible')
       //     //Trash Icon
       //     cy.get('[href="https://integration-uni.levelaccess.io/local/accesscohort/edit.php?id=209&returnurl=%2Flocal%2Faccesscohort%2Findex.php%3Fpage%3D0&delete=1"] > .icon').should('be.visible')
       //     //Gears Icon 
       //     cy.get('[href="https://integration-uni.levelaccess.io/local/accesscohort/edit.php?id=209&returnurl=%2Flocal%2Faccesscohort%2Findex.php%3Fpage%3D0"]').should('be.visible')
       //     //Group Icon
       //     cy.get('[href="https://integration-uni.levelaccess.io/local/accesscohort/assign.php?id=209&returnurl=%2Flocal%2Faccesscohort%2Findex.php%3Fpage%3D0"] > .icon').should('be.visible')
    })

});