/// <reference types="cypress" />

context('Location', () => {

    before(() => {
        Cypress.Cookies.preserveOnce('MoodleSession', 'remember_token')
        cy.visit('https://integration-uni.levelaccess.io/login/index.php');
        Cypress.Cookies.preserveOnce('MoodleSession','remember_token')
        cy.wait(3000)
        cy.SiteAdminLogin({email: 'qadirectsiteadmin@levelaccess.com', password: 'QADSAPass#1'})
        
  
    })

 it ('Verify Left Navigation Links: Reports, My Report, Cohort, Upload users. Organization, Edit Cohorts, Cohort Mapping, Bulk edit Cohorts, Configure Organization admins, My Courses', () => {
        cy.get('.page-header-headings > p').should('have.text','Your organization has made the courses below available to you to give you the digital accessibility knowledge you need, when you need it. Select any course on your dashboard below to start learning.')
        cy.get('.fa-bars').click()
        cy.wait(2000)
        cy.get('a[href=\'https://integration-uni.levelaccess.io/local/access_level_org_report/reports.php\'] > .m-l-0').click()
        cy.get('.fheader').should('contain','Level Access')
        cy.wait(2000)
        cy.get('[href="https://integration-uni.levelaccess.io/local/access_level_org_report/report/access_level_user_report.php"] > .m-l-0').click()
        cy.get('h1').should('be.visible')
        cy.get('[href="https://integration-uni.levelaccess.io/local/accesscohort/index.php"]').should('be.visible')
        cy.get('[href="https://integration-uni.levelaccess.io/local/accesscohort/index.php"]').click()
        cy.wait(3000)
        cy.get('[href="https://integration-uni.levelaccess.io/admin/tool/uploadaccessuser/index.php"]').should('be.visible')
        cy.get('[href="https://integration-uni.levelaccess.io/admin/tool/uploadaccessuser/index.php"]').click()   
        cy.get('h1').should('be.visible')
        cy.get('h1').should('have.text','Access University')
        cy.get('.fheader').should('be.visible').and('have.text','Upload')
        //Org 
        cy.get('[href="https://integration-uni.levelaccess.io/local/accesscohort/list_org.php"]').click()
        cy.get('h2').should('have.text','List of organization')
        //Edit Cohorts 
        cy.get('[href="https://integration-uni.levelaccess.io/local/accesscohort/enrolment_present.php"]').click()
        //Cohort Mapping 
        cy.get('[href="https://integration-uni.levelaccess.io/local/accesscohort/list_mapping.php"]').click()
        //Bulk Edit cohorts 
        cy.get('[href="https://integration-uni.levelaccess.io/local/accesscohort/bulk_edit_cohorts.php"]').click()
        //cy.get('h1').should('be.visilbe')
        //Config Org Admins 
        cy.get('[href="https://integration-uni.levelaccess.io/local/accesscohort/list_admin.php"]').click()
        cy.get('h2').should('be.visible')
        //universirty 
        cy.get('[href="https://integration-uni.levelaccess.io/my"]').click()
        cy.get('#inst242 > .card-block').should('be.visible')

     })

    


});


