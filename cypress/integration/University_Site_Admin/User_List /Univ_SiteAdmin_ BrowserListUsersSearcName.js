/// <reference types="cypress" />

context('Location', () => {

    before(() => {
        Cypress.Cookies.preserveOnce('MoodleSession', 'remember_token')
        cy.visit('https://integration-uni.levelaccess.io/login/index.php');
        cy.wait(3000)
        cy.SiteAdminLogin({email: 'qadirectsiteadmin@levelaccess.com', password: 'QADSAPass#1'})
        Cypress.Cookies.preserveOnce('MoodleSession','remember_token')
  
    })

    it ('Site Admin: User List: Search on User Name "Anders"' , () => {
        // if cy.get('#nav-drawer'). is not visible . then click  => cy.get('.fa-bars').click()

        cy.get('a[data-key=\'sitesettings\'] > .m-l-0').then(($SiteLink) => {

         if ($SiteLink.text().includes('Site administtration')) {
             //cy.get('a[data-key=\'sitesettings\'] > .m-l-0').should('be.visible')
             cy.get('a[data-key=\'sitesettings\'] > .m-l-0').click({force: true})
            }
         else {  cy.get('.fa-bars').click()
                 cy.get('a[data-key=\'sitesettings\'] > .m-l-0').click({force:true})   
        }
        })
        
        cy.get('[role="main"] > .nav').children().should('have.length', 9)
        cy.wait(2000)
         cy.get('h1').should('have.text','Access University')
         cy.get(':nth-child(2) > .nav-link').should('have.text','Users')
         cy.wait(2000)
         cy.get(':nth-child(2) > .nav-link').click() 
         cy.get('#linkusers > .card > .card-block > .container > :nth-child(1) > .col-sm-3 > :nth-child(1) > a').should('be.visible')
         cy.get('#linkusers > .card > .card-block > .container > :nth-child(3) > .col-sm-9 > .list-unstyled > :nth-child(1) > a').should('have.text','Browse list of users')
         cy.get('#linkusers > .card > .card-block > .container > :nth-child(3) > .col-sm-9 > .list-unstyled > :nth-child(1) > a').click()
         cy.get('h2').contains('Users')
         //New Filter is present 
        cy.get('.fcontainer').should('be.visible')
        cy.get('.fheader').should('have.text','New filter')
        cy.get('#id_realname_op').should('be.visible')

         // Edit Row and Icons
         //cy.get('thead > tr > .c6').should('Edit')
         //cy.get('tbody > :nth-child(1) > .c6').should('be.visible')
        //Searching for a user using contains    
        cy.wait(2000) 
        cy.get('#id_realname').click({ force:true })
        cy.get('#id_realname').type('Anders')
        cy.get('#id_addfilter').click({ force:true })
        cy.wait(3000)
         //cy.get(':nth-child(1) > .col-md-9 > label').should('have.text','User full name contains "Anders"')
        //cy.get('#id_actfilterhdr > .ftoggler > .fheader').should('have.text','Active filters')
        cy.get('#id_removeselected').should('be.visible')
        cy.get('#id_removeall').should('be.visible')





        })

     

       





});