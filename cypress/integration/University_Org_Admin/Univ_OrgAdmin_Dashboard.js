/// <reference types="cypress" />

context('Location', () => {

    before(() => {
        Cypress.Cookies.preserveOnce('MoodleSession', 'remember_token')
        cy.visit('https://integration-uni.levelaccess.io/login/index.php');
        cy.wait(3000)
        cy.login({email: 'qadirectorgadmin@levelaccess.com', password:'QADOAPass#1'})
        
  
    })

    it ('Inspect Main Page: verify Welcome Message and Verify logged in', () => {
      cy.get('h1').should('have.text', 'Welcome, QADirect OrgAdmin')
      cy.get('.page-header-headings > p').should('have.text','Your organization has made the courses below available to you to give you the digital accessibility knowledge you need, when you need it. Select any course on your dashboard below to start learning.')
       // cy.visit('https://integration-uni.levelaccess.io/my/')
    })

 it ('Inspect Left Navigation:20 Links :  Site Admin Links Display: Cohort Mapping, Bulk Edit Cohorts, Cdnfigure Org Admin and Site Admin link', () => {
        cy.wait(1000)
        cy.get('.fa-bars').click()
        cy.get('a[href=\'https://integration-uni.levelaccess.io/local/access_level_org_report/reports.php\'] > .m-l-0').should.apply('contain','Reports')
        cy.get('a[href=\'https://integration-uni.levelaccess.io/local/access_level_org_report/report/access_level_user_report.php\'] > .m-l-0').should('contain', 'My report')
        cy.get('[href="https://integration-uni.levelaccess.io/local/accesscohort/index.php"]').should('contain','Cohort')
        cy.get('a[href=\'https://integration-uni.levelaccess.io/admin/tool/uploadaccessuser/index.php\'] > .m-l-0').should('contain', 'Upload users')
        cy.get('a[href=\'https://integration-uni.levelaccess.io/local/accesscohort/list_org.php\'] > .m-l-0').should('contain', 'organization')
        cy.get('a[href=\'https://integration-uni.levelaccess.io/local/accesscohort/enrolment_present.php\'] > .m-l-0').should('contain','Edit Cohorts')
        cy.get('a[href=\'https://integration-uni.levelaccess.io/local/accesscohort/list_mapping.php\']').should('contian','Cohort Mapping')
        cy.get('a[href=\'https://integration-uni.levelaccess.io/local/accesscohort/bulk_edit_cohorts.php\'] > .m-l-0').should('contain', 'Bulk edit Cohorts')
        cy.get('a[href=\'https://integration-uni.levelaccess.io/local/accesscohort/list_admin.php\'] > .m-l-0').should('contain','Configure organization admins')
        cy.get('a[href=\'https://integration-uni.levelaccess.io/my\'] > .m-l-0').should('contain', 'My Courses')
        
        
        cy.get('a[data-key=\'localboostnavigationcustombottomusers1\'] > .m-l-0').should('contain','How to use Access University')
        cy.get('a[data-key=\'localboostnavigationcustombottomusers2\'] > .m-l-0').should('contain', 'Accessibility Awareness')
        cy.get('a[data-key=\'localboostnavigationcustombottomusers3\'] > .m-l-0').should('contain', 'Testing Tools and Assistive Technologies')
        cy.get('a[data-key=\'localboostnavigationcustombottomusers4\'] > .m-l-0').should('contain','Document Accessibility')
        cy.get('a[data-key=\'localboostnavigationcustombottomusers5\'] > .m-l-0').should('contain','Mobile Accessibility')
        cy.get('a[data-key=\'localboostnavigationcustombottomusers6\'] > .m-l-0').should('contain', 'Web Accessibility')
        cy.get('a[data-key=\'localboostnavigationcustombottomusers7\'] > .m-l-0').should('contain','Accessibility Policy and Regulations')
        cy.get('a[data-key=\'localboostnavigationcustombottomusers8\'] > .m-l-0').should('contain','Level Access Products')
    })
     
      it('Course Search search field displays with correct text',() => {
        cy.get('#inst118 > .card-block').should('be.visible')
        cy.get('#instance-118-header').should('have.text','Course search')
        cy.get('#shortsearchbox').should('be.visible')

      })

      it('Main Content Course Cards: card titles display, images present and paragraph displays', () => {
        cy.get('.lw_courses_list').should('be.visible')
        cy.get('h2').should('have.text','My Courses') 
        cy.get('#course-2 > .course_title > .title > a').should('have.text','Accessibility Concepts v2.0')
        cy.get('#course-26 > .course_title > .title > a').should('have.text','Introduction to the CVAA')
        cy.get('#course-24 > .image_wrap > .course_image').should('be.visible')
      })


    it('My Course Listing Displays at the bottom of the page', () => {
      cy.get('#inst46 > .card-block').should('be.visible')
      cy.get(':nth-child(1) > .column > a').should('be.visible')
      cy.get(':nth-child(2) > .column > a').should('be.visible')
      cy.get(':nth-child(3) > .column > a').should('be.visible')
      cy.get(':nth-child(4) > .column > a').should('be.visible')
      cy.get(':nth-child(5) > .column > a').should('be.visible')
      cy.get(':nth-child(6) > .column > a').should('be.visible')
      }) 

  it('Verification that the footer displays at the bottom of the page and logged in as QADirect SiteAdmin',()=> {
    cy.get('.container').should('be.visible')
    cy.get('.logininfo').should('be.visible')
    cy.get('.logininfo').should('have.text','You are logged in as s QADirect SiteAdmin (Log out)')
    cy.get('.la-footer-left').should('contain','1600 Spring Hill Rd')    
  })

});


